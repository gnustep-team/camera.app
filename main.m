
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL

#include "AppDelegate.h"

#include <AppKit/NSApplication.h>

/*
 * Initialize and go!
 */
int main(int argc, const char *argv[]) 
{
   int          apprc;
   AppDelegate* delegate;

   [NSApplication sharedApplication];

   delegate = [[AppDelegate alloc] init];
   [[NSApplication sharedApplication] setDelegate: delegate];

   apprc = NSApplicationMain (argc, argv);

   [delegate release];
   return apprc;
}
