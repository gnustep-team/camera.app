
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL


#include "AppDelegate.h"


/*
 * Non-Public methods.
 */
@interface AppDelegate(Private)
@end


/*
 * Camera's delegate.
 */
@implementation AppDelegate

- (id) init
{
   if ((self = [super init]))
   {
      // ...
   }
   return self;
}


- (void) dealloc
{
   [super dealloc];
}


- (void) applicationDidFinishLaunching: (NSNotification*) notification
{
   [[NSApplication sharedApplication] setServicesProvider: self];
   NSLog(@"Service provider registered");
}


/*
 * A service that downloads all files from the camera
 * to a directory.
 */
- (void) downloadFilesToPlace: (NSPasteboard*)pboard
                     userData: (NSString*)userData
                        error: (NSString**)error
{
   NSString* directory;

   NSLog(@"Service downloadFilesToPlace invoked");

   directory = [pboard stringForType: NSFilenamesPboardType];
   if (!directory)
   {
      *error = @"No directory selected.";
      return;
   }

   NSLog(@"downloading files to %@", directory);
}

@end
