
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL

#ifndef _H_APPDELEGATE
#define _H_APPDELEGATE

#include <Foundation/NSObject.h>
#include <Foundation/NSNotification.h>
#include <AppKit/NSApplication.h>
#include <AppKit/NSPasteboard.h>


@interface AppDelegate : NSObject
{
}

- (id) init;
- (void) dealloc;

- (void) applicationDidFinishLaunching: (NSNotification*) notification;

// Services
- (void) downloadFilesToPlace: (NSPasteboard*) pboard
                     userData: (NSString*) userData
                        error: (NSString**) error;

@end

#endif
