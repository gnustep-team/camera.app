
// created 11.2003 by Stefan Kleine Stegemann
// 
// licensed under GPL

#ifndef _H_CAMERA_CONTROLLER
#define _H_CAMERA_CONTROLLER

#include <Foundation/NSObject.h>
#include <Foundation/NSString.h>
#include <Foundation/NSLock.h>
#include <AppKit/NSImage.h>

#include "DigitalCamera.h"


extern NSString* PREF_DOWNLOAD_BASE_PATH;
extern NSString* PREF_USE_TIMESTAMP_DIR;


@interface CameraController : NSObject
{
   DigitalCamera*    selectedCamera;
   BOOL              downloadIsActive;
   
   // Outlets
   id  deleteFilesAfterDownload;
   id  progressInfoMsg;
   id  progressBar;
   id  thumbnailView;
   id  transferButton;
   id  cameraInfo;
   id  cameraIcon;
   id  window;
}

- (id) init;
- (void) dealloc;

- (void) awakeFromNib;

- (void) setSelectedCamera: (DigitalCamera*)aCamera;
- (DigitalCamera*) selectedCamera;
- (void) setDownloadIsActive: (BOOL)active;
- (BOOL) downloadIsActive;

// Notifications
- (void) willDownloadFile: (DigitalCameraFile*)file
                       at: (int)index
                       of: (int)total
                thumbnail: (NSImage*)thumbnail;

- (void) willDeleteFile: (DigitalCameraFile*)file
                     at: (int)index
                     of: (int)total;

- (void) downloadFinished;


// Actions
- (void) detectCamera: (id)sender;
- (void) initiateDownloadFiles: (id)sender;
- (void) abortDownloadFiles: (id)sender;
- (void) initiateOrAbortDownload: (id)sender;
- (void) setDestination: (id)sender;

@end

#endif
